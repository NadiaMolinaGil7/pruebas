package com.curso.panel.herencia;

public class App {

	public static void main(String[] args) {
	
		Empleado e1 = new Empleado("Iv�n");
		Directivo d1 = new Directivo("Nizar");
		Operario o1= new Operario("Sergio");
		Oficial of1= new Oficial("A�da");
		Tecnico t1= new Tecnico("Silvina");
		
		System.out.println(e1);
		System.out.println(d1);
		System.out.println(o1);
		System.out.println(of1);
		System.out.println(t1);
		

	}

}
