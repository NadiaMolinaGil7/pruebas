package com.curso.panel.herencia;
//Comentario
public class Directivo extends Empleado {

	
	
	public Directivo() {
		
	}

	public Directivo(String nombre) {
		super(nombre);
		System.out.println("Constructor clase Directivo");
	}

	@Override
	public String toString() {
		return super.toString() + "  -> Directivo";
	}
	
	
	

	
}
